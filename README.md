# purs-counter

In project is used [Elmish](https://pursuit.purescript.org/packages/purescript-elmish/0.10.0/docs/Elmish)

* `Counter.purs` - base example, demonstrating state + two messages


## Building and running

* To initialize: `npm install`
* To build: `npm run build`
* To start a dev server with autoreloading changes: `npm start`
