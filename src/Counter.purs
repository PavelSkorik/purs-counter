module Counter( def, Message(..), State ) where

import Prelude
import Elmish (ComponentDef, (<|))
import Elmish.HTML.Styled as H

data Message = Inc | Dec

type State = { count :: Int }

def :: ComponentDef Message State
def =
  { init: pure { count: 0 }
  , update
  , view
  }
  where
    update s Inc = pure s { count = s.count + 1 }
    update s Dec = pure s { count = s.count - 1 }

    view s dispatch = 
      H.div ""
        [ 
          H.div "" $ show s.count
        , H.button_ "" { onClick: dispatch <| Inc } "+"
        , H.button_ "" { onClick: dispatch <| Dec } "-"
        ]