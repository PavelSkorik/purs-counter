module Main
  ( main
  )
  where

import Prelude
import Counter as Counter
import Effect (Effect)
import Elmish.Boot as Boot
main :: Effect Unit
main = do
  Boot.defaultMain { elementId: "app", def: Counter.def }