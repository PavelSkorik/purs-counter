{ sources =
    [ "src/**/*.purs", "test/**/*.purs" ]
, name =
    "purs-counter"
, dependencies =
    [ 
      "effect"
    , "elmish-html"
    , "elmish"
    , "prelude"
    ]
, packages =
    ./packages.dhall
}
